@SearchForBusiness
Feature: User is able to search for businesses with different names and locations

  Scenario: Search for an existing company finds it by name
    When AuthorisedUser searches businesses with location "New York" and name "Admirable Jewels"
    Then AuthorisedUser can see that business with name "Admirable Jewels" is found

  Scenario: Search for a non existing name is returning an empty results response
    When AuthorisedUser searches businesses with location "New York" and name "9938478383"
    Then AuthorisedUser can see that no businesses are found