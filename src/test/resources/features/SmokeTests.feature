@Smoke
Feature: Search for businesses endpoint is available

  Scenario: Search for companies with an authorised user
    When AuthorisedUser searches businesses with location "Dublin" and name "name"
    Then AuthorisedUser can see that response code is 200

  Scenario: Search for companies with an user not authorised
    When NonAuthorisedUser searches businesses with location "Dublin" and name "name"
    Then NonAuthorisedUser can see that response code is 400