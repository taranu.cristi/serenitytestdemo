package com.serenity.demo.rest.interactions;

import com.serenity.demo.screenplay.abilities.UseAuthentication;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.serenitybdd.screenplay.rest.interactions.RestInteraction;
import net.serenitybdd.screenplay.rest.questions.LastResponse;

public class GetWithAuthentication extends RestInteraction {
    private final String resource;

    public GetWithAuthentication(String resource) {
        this.resource = resource;
    }

    public static GetWithAuthentication resource(String resource) {
        return Instrumented.instanceOf(GetWithAuthentication.class).withProperties(resource);
    }

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Get.resource(this.resource)
                .with(requestSpecification -> authenticatedSpecification(actor)));
        actor.asksFor(LastResponse.received());
    }

    private RequestSpecification authenticatedSpecification(Actor actor) {
        RequestSpecification requestSpecification = rest();
        if (actor.abilityTo(UseAuthentication.class) != null) {
            return requestSpecification.header("Authorization", "Bearer " + UseAuthentication.as(actor).getToken());
        }
        return requestSpecification;
    }
}