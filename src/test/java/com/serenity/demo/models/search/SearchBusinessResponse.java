package com.serenity.demo.models.search;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SearchBusinessResponse{

	@SerializedName("total")
	private Integer total;

	@SerializedName("region")
	private Region region;

	@SerializedName("businesses")
	private List<BusinessesItem> businesses;

	public void setTotal(Integer total){
		this.total = total;
	}

	public Integer getTotal(){
		return total;
	}

	public void setRegion(Region region){
		this.region = region;
	}

	public Region getRegion(){
		return region;
	}

	public void setBusinesses(List<BusinessesItem> businesses){
		this.businesses = businesses;
	}

	public List<BusinessesItem> getBusinesses(){
		return businesses;
	}
}