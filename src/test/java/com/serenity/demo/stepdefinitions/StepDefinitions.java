package com.serenity.demo.stepdefinitions;

import com.serenity.demo.screenplay.tasks.SearchBusinesses;
import com.serenity.demo.screenplay.tasks.assertions.AssertThatBusinessIsFound;
import com.serenity.demo.screenplay.tasks.assertions.AssertThatNoBusinessesAreFound;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

public class StepDefinitions {
    private static Logger logger = LoggerFactory.getLogger(StepDefinitions.class);

    @When("{actor} searches businesses with location {string} and name {string}")
    public void searchBusinesses(Actor actor, String location, String searchTerm) {
        actor.attemptsTo(SearchBusinesses.withSearchTerm(searchTerm).withLocation(location));
    }

    @Then("{actor} can see that response code is {int}")
    public void assertLastResponseCode(Actor actor, int expectedResponseCode) {
        assertThat(actor.asksFor(LastResponse.received()).statusCode())
                .isEqualTo(expectedResponseCode);
    }

    @Then("{actor} can see that business with name {string} is found")
    public void assertThatBusinessIsFound(Actor actor, String businessNameExpected) {
        actor.attemptsTo(AssertThatBusinessIsFound.withName(businessNameExpected));
    }

    @Then("{actor} can see that no businesses are found")
    public void assertThatNoBusinessesAreFound(Actor actor) {
        actor.attemptsTo(AssertThatNoBusinessesAreFound.inLastResponse());
    }
}