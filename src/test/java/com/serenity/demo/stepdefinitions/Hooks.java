package com.serenity.demo.stepdefinitions;

import com.serenity.demo.screenplay.abilities.UseAuthentication;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import static org.assertj.core.api.Assertions.assertThat;

public class Hooks {

    @Before(order = 0)
    public void setupTheStage() {
        new SystemEnvironmentVariables();
        EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
        OnStage.setTheStage(new OnlineCast());
        assertThat(environmentVariables.getProperty("restapi.base.url"))
                .withFailMessage("Please define the base url: restapi.base.url")
                .isNotNull();
        assertThat(environmentVariables.getProperty("api.key"))
                .withFailMessage("Please add the api.key")
                .isNotNull();
        OnStage.theActorCalled("AuthorisedUser")
                .whoCan(CallAnApi.at(environmentVariables.getProperty("restapi.base.url")))
                .whoCan(UseAuthentication.withToken(environmentVariables.getProperty("api.key")));
        OnStage.theActorCalled("NonAuthorisedUser")
                .whoCan(CallAnApi.at(environmentVariables.getProperty("restapi.base.url")));
    }

    @After
    public void tearDown() {
        OnStage.drawTheCurtain();
    }
}