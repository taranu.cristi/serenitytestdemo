package com.serenity.demo.screenplay.qestions;

import com.serenity.demo.models.search.SearchBusinessResponse;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LastSearchBusinessResponse implements Question<SearchBusinessResponse> {

    public static LastSearchBusinessResponse inThisSession() {
        return Instrumented.instanceOf(LastSearchBusinessResponse.class).newInstance();
    }

    @Override
    public SearchBusinessResponse answeredBy(Actor actor) {
        assertThat(SerenityRest.lastResponse().statusCode())
                .withFailMessage("Invalid response code returned,didn't attempt to deserialize the response")
                .isEqualTo(200);
        return SerenityRest.lastResponse().as(SearchBusinessResponse.class);
    }
}