package com.serenity.demo.screenplay.tasks.assertions;

import com.serenity.demo.models.search.SearchBusinessResponse;
import com.serenity.demo.screenplay.qestions.LastSearchBusinessResponse;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AssertThatNoBusinessesAreFound implements Performable {

    public static AssertThatNoBusinessesAreFound inLastResponse() {
        return Instrumented.instanceOf(AssertThatNoBusinessesAreFound.class).newInstance();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        SearchBusinessResponse response = actor.asksFor(LastSearchBusinessResponse.inThisSession());
        assertThat(response.getTotal())
                .withFailMessage("Search response didn't return an empty list")
                .isEqualTo(0);
        assertThat(response.getBusinesses())
                .withFailMessage("Total items is returned as 0 but businesses list is not empty")
                .asList()
                .isEmpty();
    }
}