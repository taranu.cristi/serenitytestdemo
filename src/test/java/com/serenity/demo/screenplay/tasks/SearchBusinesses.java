package com.serenity.demo.screenplay.tasks;

import com.serenity.demo.rest.interactions.GetWithAuthentication;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;

import static com.serenity.demo.endpoints.Endpoints.GET_SEARCH_BUSINESSES;
import static net.serenitybdd.rest.SerenityRest.rest;

public class SearchBusinesses implements Performable {
    private String location;
    private String term;
    private Double latitude;
    private Double longitude;
    private Integer radius;
    private String categories;
    private String locale;
    private Integer limit;
    private Integer offset;
    private String sort_by;
    private String price;
    private Boolean open_now;
    private Integer open_at;
    private String attributes;

    public SearchBusinesses(String location, String term, Double latitude, Double longitude, Integer radius, String categories, String locale, Integer limit, Integer offset, String sort_by, String price, Boolean open_now, Integer open_at, String attributes) {
        this.location = location;
        this.term = term;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.categories = categories;
        this.locale = locale;
        this.limit = limit;
        this.offset = offset;
        this.sort_by = sort_by;
        this.price = price;
        this.open_now = open_now;
        this.open_at = open_at;
        this.attributes = attributes;
    }

    public static Builder withSearchTerm(String term) {
        return new Builder(term);
    }

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(GetWithAuthentication.resource(GET_SEARCH_BUSINESSES.path())
                .with(requestSpecification -> requestParams()
                        .contentType(ContentType.JSON)));
    }

    private RequestSpecification requestParams() {
        RequestSpecification request = rest();

        if (location != null) {
            request.queryParam("location", location);
        }
        if (term != null) {
            request.queryParam("term", term);
        }
        if (latitude != null) {
            request.queryParam("latitude", latitude);
        }
        if (longitude != null) {
            request.queryParam("longitude", longitude);
        }
        if (radius != null) {
            request.queryParam("radius", radius);
        }
        if (categories != null) {
            request.queryParam("categories", radius);
        }
        if (locale != null) {
            request.queryParam("locale", radius);
        }
        if (limit != null) {
            request.queryParam("limit", limit);
        }
        if (offset != null) {
            request.queryParam("offset", offset);
        }
        if (sort_by != null) {
            request.queryParam("sort_by", sort_by);
        }
        if (price != null) {
            request.queryParam("price", price);
        }
        if (open_now != null) {
            request.queryParam("open_now", open_now);
        }
        if (open_at != null) {
            request.queryParam("open_at", open_at);
        }
        if (attributes != null) {
            request.queryParam("attributes", attributes);
        }
        return request;
    }

    public static class Builder {
        private String term;
        private Double latitude;
        private Double longitude;
        private Integer radius;
        private String categories;
        private String locale;
        private Integer limit;
        private Integer offset;
        private String sort_by;
        private String price;
        private Boolean open_now;
        private Integer open_at;
        private String attributes;

        public Builder(String term) {
            this.term = term;
        }

        public Builder withLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder withLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder withRadius(Integer radius) {
            this.radius = radius;
            return this;
        }

        public Builder withCategories(String categories) {
            this.categories = categories;
            return this;
        }

        public Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public Builder withLimit(Integer limit) {
            this.limit = limit;
            return this;
        }

        public Builder withOffset(Integer offset) {
            this.offset = offset;
            return this;
        }

        public Builder withSort_by(String sort_by) {
            this.sort_by = sort_by;
            return this;
        }

        public Builder withPrice(String price) {
            this.price = price;
            return this;
        }

        public Builder withOpen_now(Boolean open_now) {
            this.open_now = open_now;
            return this;
        }

        public Builder withOpen_at(Integer open_at) {
            this.open_at = open_at;
            return this;
        }

        public Builder withAttributes(String attributes) {
            this.attributes = attributes;
            return this;
        }

        public SearchBusinesses withLocation(String location) {
            return Instrumented.instanceOf(SearchBusinesses.class).withProperties(location, term, latitude, longitude, radius, categories, locale, limit, offset, sort_by, price, open_now, open_at, attributes);
        }
    }
}