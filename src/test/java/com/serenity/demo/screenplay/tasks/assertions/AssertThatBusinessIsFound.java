package com.serenity.demo.screenplay.tasks.assertions;

import com.serenity.demo.models.search.SearchBusinessResponse;
import com.serenity.demo.screenplay.qestions.LastSearchBusinessResponse;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;

public class AssertThatBusinessIsFound implements Performable {

    private String expectedBusinessName;

    public AssertThatBusinessIsFound(String expectedBusinessName) {
        this.expectedBusinessName = expectedBusinessName;
    }

    public static AssertThatBusinessIsFound withName(String expectedBusinessName) {
        return Instrumented.instanceOf(AssertThatBusinessIsFound.class).withProperties(expectedBusinessName);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        SearchBusinessResponse response = actor.asksFor(LastSearchBusinessResponse.inThisSession());
        response.getBusinesses().stream()
                .filter(business -> business.getName().equals(expectedBusinessName))
                .findFirst()
                .orElseThrow(() -> new AssertionError("Couldn't find a business with the expected name in the response"));
    }
}