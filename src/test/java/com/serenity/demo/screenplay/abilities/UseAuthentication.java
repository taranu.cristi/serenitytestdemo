package com.serenity.demo.screenplay.abilities;

import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

public class UseAuthentication implements Ability {

    private final String token;

    private UseAuthentication(String token) {
        this.token = token;
    }

    public static UseAuthentication withToken(String token) {
        return new UseAuthentication(token);
    }

    public static UseAuthentication as(Actor actor) {
        return actor.abilityTo(UseAuthentication.class);
    }

    public String getToken() {
        return this.token;
    }
}