package com.serenity.demo.endpoints;

public enum Endpoints {
    GET_SEARCH_BUSINESSES("/businesses/search"),
    GET_SEARCH_PHONE("/businesses/search/phone"),
    GET_SEARCH_TRANSACTIONS("/transactions/{transaction_type}/search"),
    GET_BUSINESS_DETAILS("/businesses/{id}"),
    GET_BUSINESS_MATCH("/businesses/matches"),
    GET_REVIEWS("/businesses/{id}/reviews"),
    GET_AUTOCOMPLETE("/autocomplete");

    private String path;

    Endpoints(String path) {
        this.path = path;
    }

    public String path() {
        return this.path;
    }
}