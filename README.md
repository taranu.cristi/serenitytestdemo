# SerenityTestDemo

###Running the tests
The easiest way to run full test suite is to run maven command:
    
    mvn clean verify
    
this will also automatically generate a report in target/siteserenity/index.html

In case a more specific run is required, this can be achieved by running tests based on
the tags from feature files

    mvn clean verify -Dtags=@Smoke

where `@Smoke` is the tag that can be from a specific scenario or an entire feature file

###Logging 
####Console Logging 
By default log level is Info but this can be changed for every test run by adding 
the `-DLOG_LEVEL=<LEVEL>` to the run command where `<LEVEL>` is the required log level.

####File Logging 
The project is configured to save the log also into a log file with default location
at target/logging/appLog.log 
In case the default log file location needs to be changed this can be done by adding
`-DlogFilePath=<newLogFilePath>/myLog.log` to the run command

###API under test
This project is configured to test the Yelp Fusion API. The documentation regarding 
API requirements and specifications can be found at https://www.yelp.com/developers/documentation/v3/
under _Business Endpoints_ section.

Endpoints require an api key which is defined in `serenity.properties` file. In case the key
expires a new one can be requested by registering at https://www.yelp.com/developers

###Adding tests and improving the project
The project is using the Screenplay pattern. Currently there are 2 defined actors `AuthorisedUser` and `NonAuthorisedUser`
 AuthorisedUser - has the ability to call all the endpoints with the api key
 NonAuthorisedUser - can't use the api key
More actors can be added if required.

####Current project structure
/endpoints - contains an enum with paths for each endpoint under test

/models - contains java model classes for serialization/deserialization of endpoint's requests/responses

/rest - contains all custom rest interaction classes

/runners - contains junit runner classes with all configurations required (currently there is on general runner 
but can have multiple, for separate features/scenarios)

/screenplay - contains all the abilities, questions and tasks that actors are able to use

/stepdefinitions - contains all step definitions classes and test hooks     

/resources/features - contains all feature files

resources/logback.xml - logging configuration file

serenity.properties - serenity configuration file

####Actions required to add new tests
At this stage some basic steps are provide only for `/businesses/search` endpoint. In order to add
new tests for this endpoint one can use the `SearchBusinesses` task to make calls with different parameters.
The `LastSearchBusinessResponse` question can provide the deserialized response to be used for assertions.

The same approach can be used to add tests for other endpoints:
- add model for the response in `/models`
- add task to make the call with or without all available parameters
- add the step definitions for new steps under `/stepdefinitions`
In case later there is an endpoint that uses other REST method than Get one can define another custom
rest interaction class to use the api key, the same as `GetWithAuthentication` does.